import argparse
import random
import csv

parser = argparse.ArgumentParser(description='DetoxChat')
parser.add_argument("-i", "--input", type=str, help="Input str. Generate random float {toxicity possibility}, {avg toxicity in text}, {AI possibility}")

args = parser.parse_args()
max_toxicity = 0.0
avg_toxicity = []
string_list = list(args.input)

#search another lang char:
with open("data/another-lang-char.csv", encoding='utf-8') as lang:
    another_lang = csv.DictReader(lang, delimiter = ';')
    for row in another_lang:
        char_position = 0
        for char in string_list:
            if char == row['replace']:
                #print(char_position)
                #print("replace "+str(row['replace'])+" to "+str(row['char']))
                string_list[char_position] = row['char']
            char_position=char_position+1
result = ''.join(string_list)

result_replace = 0
with open("data/rep-char.csv", "r") as replace:
    rep = csv.DictReader(replace, delimiter = ';')
    for row in rep:
        char_position = 0
        for char in result:
            if char == row['replace']:
                #print(char_position)
                #print("replace "+str(row['replace'])+" to "+str(row['char']))
                string_list[char_position] = row['char']
            char_position=char_position+1
result_replace = ''.join(string_list)

with open("data/word-data-en.csv", "r") as file:
    swear = csv.DictReader(file, delimiter=';')
    for row in swear:
        #print(row)
        for word in result_replace.split():
        #print(row['id'], row['word'], row['toxicity'])
            if word == str(row['word']):
                #print(row['word']+" in "+row['id']+";")
                avg_toxicity.append(float(row['toxicity']))
                if max_toxicity<float(row['toxicity']):
                    max_toxicity = float(row['toxicity'])
if not avg_toxicity:
    avg_toxicity = 0.0
else:
    avg_toxicity = sum(avg_toxicity)/len(avg_toxicity)

print(str(max_toxicity)+", "+str(avg_toxicity)+", "+str(random.random()))
