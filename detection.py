import pathlib
import random


class ToxicityRating:
    def __init__(self, message_severity: float, detection_accuracy: float):
        self.message_severity = message_severity
        self.detection_accuracy = detection_accuracy

    def merge(self, other: 'ToxicityRating', my_weight: float, other_weight: float) -> 'ToxicityRating':
        my_ratio = my_weight / (my_weight + other_weight)
        other_ratio = other_weight / (my_weight + other_weight)
        return ToxicityRating(
            self.message_severity * my_ratio + other.message_severity * other_ratio,
            self.detection_accuracy * my_ratio + other.detection_accuracy * other_ratio,
        )


class AbstractMessageInspector:
    def analyze(self, message: str) -> ToxicityRating:
        raise NotImplementedError()


class MessageInspectorV1(AbstractMessageInspector):
    def analyze(self, message: str) -> ToxicityRating:
        return ToxicityRating(random.random(), random.random())


class MessageInspectorV2(AbstractMessageInspector):
    def __init__(self, path_to_data: pathlib.Path):
        self.path_to_data = path_to_data

    def analyze(self, message: str) -> ToxicityRating:
        import csv
        max_toxicity = 0.0
        avg_toxicity = []
        string_list = list(message)
        with open(self.path_to_data.joinpath("another-lang-char.csv"), encoding='utf-8') as lang:
            another_lang = csv.DictReader(lang, delimiter = ';')
            for row in another_lang:
                char_position = 0
                for char in string_list:
                    if char == row['replace']:
                        string_list[char_position] = row['char']
                    char_position=char_position+1
        result = ''.join(string_list)

        result_replace = ""
        with open(self.path_to_data.joinpath("rep-char.csv"), "r") as replace:
            rep = csv.DictReader(replace, delimiter = ';')
            for row in rep:
                char_position = 0
                for char in result:
                    if char == row['replace']:
                        string_list[char_position] = row['char']
                    char_position=char_position+1
        result_replace = ''.join(string_list)

        with open(self.path_to_data.joinpath("word-data-en.csv"), "r") as file:
            swear = csv.DictReader(file, delimiter=';')
            for row in swear:
                #print(row)
                for word in result_replace.split():
                    if word == str(row['word']):
                        avg_toxicity.append(float(row['toxicity']))
                        if max_toxicity<float(row['toxicity']):
                            max_toxicity = float(row['toxicity'])
        if not avg_toxicity:
            avg_toxicity = 0.0
        else:
            avg_toxicity = sum(avg_toxicity)/len(avg_toxicity)
        return ToxicityRating(max_toxicity, avg_toxicity)


class MessageInspectorV3(AbstractMessageInspector):
    API_KEY = 'AIzaSyCDEBjgNLP1tP_xvAa7XkLmwLqcxQwPd38'

    def analyze(self, message: str) -> ToxicityRating:
        
        from googleapiclient import discovery

        client = discovery.build(
            "commentanalyzer",
            "v1alpha1",
            developerKey=self.API_KEY,
            discoveryServiceUrl="https://commentanalyzer.googleapis.com/$discovery/rest?version=v1alpha1",
            static_discovery=False,
        )

        analyze_request = {
            'comment': {'text': message},
            'languages': ['en'],
            'requestedAttributes': {'TOXICITY': {}}
        }

        response = client.comments().analyze(body=analyze_request).execute()

        return ToxicityRating(response['attributeScores']['TOXICITY']['summaryScore']['value'], 1)


class MessageInspectorV4(AbstractMessageInspector):

    def __init__(self, path_to_data: pathlib.Path):
        self.model2 = MessageInspectorV2(path_to_data)
        self.model3 = MessageInspectorV3()

    def analyze(self, message: str) -> ToxicityRating:
        v2_result = self.model2.analyze(message)
        v3_result = self.model3.analyze(message)
        return v3_result.merge(v2_result, 2, 1)
