from backend.pointscalculator import *
from backend.detection import *
import hashlib


class MessageRating:
    def __init__(self,
                 message_severity: float,
                 detection_accuracy: float,
                 sender_kindness: float,
                 min_room_kindness: float,
                 max_room_kindness: float,
                 average_room_kindness: float,
                 recipient_kindness: float,
                 recipient_sensitivity: float
                 ):
        self.recipient_sensitivity = recipient_sensitivity
        self.recipient_kindness = recipient_kindness
        self.average_room_kindness = average_room_kindness
        self.max_room_kindness = max_room_kindness
        self.min_room_kindness = min_room_kindness
        self.sender_kindness = sender_kindness
        self.detection_accuracy = detection_accuracy
        self.message_severity = message_severity


class ChatServer:
    async def announce(self, message_id: str, rating: MessageRating, data) -> None:
        raise NotImplementedError()

    async def warn_toxic(self, message_id: str, rating: MessageRating, data) -> None:
        raise NotImplementedError()


class DetoxHandler:
    def __init__(self, message_inspector: AbstractMessageInspector, server: ChatServer):
        self.points_calculator = PointsCalculator(
            ToxicityLevelSettings(),
            threshold=0.2,
            alpha=10,
        )
        self.detector = message_inspector
        self.messages = dict()
        self.chat_server = server

    def register_message(self, message: str, data) -> str:
        """
        Use this method to initiate sending one message and retrieving its message id as return value.
        The message id can be used for various different tasks and is useful for the GUI component.
        :param data: Any data
        :param message: The message to be sent
        :return: The id of the registered message
        """
        h = hashlib.sha1(message.encode("utf-8"))
        while h in self.messages:
            h = hashlib.sha1(h.encode("utf-8")).hexdigest()
        self.messages[h] = (message, data)
        return h

    def unregister_message(self, message_id: str) -> None:
        del self.messages[message_id]

    async def ignore_warning(self, message_id: str):
        message, data = self.messages[message_id]
        rating = self.make_message_rating(message)
        await self.chat_server.announce(message_id, rating, data)

    async def send_message(self, message_id: str):
        message, data = self.messages[message_id]
        rating = self.make_message_rating(message)
        if rating.message_severity * rating.detection_accuracy > 0.5:
            await self.chat_server.warn_toxic(message_id, rating, data)
        else:
            await self.chat_server.announce(message_id, rating, data)

    def get_message_text(self, message_id: str):
        return self.messages[message_id][0]

    def make_message_rating(self, message: str) -> MessageRating:
        result = self.detector.analyze(message)
        return MessageRating(
            message_severity=result.message_severity,
            detection_accuracy=result.detection_accuracy,
            sender_kindness=0.5,
            min_room_kindness=0.5,
            max_room_kindness=0.5,
            average_room_kindness=0.5,
            recipient_kindness=0.5,
            recipient_sensitivity=0.5,
        )
