from backend.detection import ToxicityRating


class ToxicityLevelSettings:
    def __init__(self):
        self.min = 1
        self.max = 5

    def map(self, z: float) -> float:
        return z / (self.max - self.min) + self.min


class PointsCalculator:
    def __init__(self, toxicity_levels: ToxicityLevelSettings, threshold: float, alpha: float):
        self.toxicity_levels = toxicity_levels
        self.threshold = threshold
        self.alpha = alpha

    def calculate(self, rating: ToxicityRating) -> float:
        score = rating.message_severity * rating.detection_accuracy
        if score < self.threshold:
            z = self.alpha / score
        else:
            z = -(self.alpha / score)
        return self.toxicity_levels.map(z)
